from collections import ChainMap

a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}

c = ChainMap(a, b)
print(c['x'])
print(c['y'])
print(c['z'])
print(list(c.keys()))
print(list(c.values()))

# Operations that mutate the mapping always affect the first mapping listed
c['z'] = 15
c['w'] = 25
print(a)
print(b)
