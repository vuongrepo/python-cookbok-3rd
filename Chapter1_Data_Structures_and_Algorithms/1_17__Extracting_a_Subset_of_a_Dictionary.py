prices = {
    'ACME': 45.23,
    'AAPL': 612.78,
    'IBM': 205.55,
    'HPQ': 37.20,
    'FB': 10.75
}

dict1 = {k: v for k, v in prices.items() if v > 200}
print(dict1)

dict2 = dict(((k, v) for k, v in prices.items() if v > 10))  # generator
print(dict2)

tech_names = {'AAPL', 'IBM', 'HPQ', 'MSFT'}
p2 = {key: prices[key] for key in prices.keys() & tech_names}
print(p2)
