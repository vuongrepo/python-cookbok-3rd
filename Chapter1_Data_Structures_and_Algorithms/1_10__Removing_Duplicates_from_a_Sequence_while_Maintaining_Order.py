def deduplicate_hashable(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)


# If you are trying to eliminate duplicates in a sequence of unhashable types (such as dicts)
def deduplicate_unhashable(items, keyfunc=None):
    seen = set()
    for item in items:
        val = item if keyfunc is None else keyfunc(item)
        if val not in seen:
            yield item
            seen.add(val)


if __name__ == '__main__':
    a = [1, 5, 2, 1, 9, 1, 5, 10]
    print(list(deduplicate_hashable(a)))
    b = [{'x': 1, 'y': 2}, {'x': 1, 'y': 3}, {'x': 1, 'y': 2}, {'x': 2, 'y': 4}]
    b_ = deduplicate_unhashable(b, keyfunc=lambda item: (item['x'], item['y']))
    print(list(b_))
