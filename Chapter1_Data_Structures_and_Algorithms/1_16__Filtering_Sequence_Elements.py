from itertools import compress

mylist = [1, 4, -5, 10, -7, 2, 3, -1]
fl1 = [x for x in mylist if x > 0]
print(fl1)

fl2 = list((x for x in mylist if x > 0))  # generator
print(fl2)


def filter_func(x):
    if x > 2:
        return True
    else:
        return False


fl3 = list(filter(filter_func, mylist))
print(fl3)

bool_list = [x > 1 for x in mylist]
fl4 = list(compress(mylist, bool_list))
print(fl4)
