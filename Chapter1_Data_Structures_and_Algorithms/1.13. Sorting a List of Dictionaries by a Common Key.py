from operator import itemgetter
import pprint

rows = [
    {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003},
    {'fname': 'David', 'lname': 'Beazley', 'uid': 1002},
    {'fname': 'John', 'lname': 'Cleese', 'uid': 1001},
    {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}
]

rows_by_lname = sorted(rows, key=itemgetter('lname'))
rows_by_fullname = sorted(rows, key=itemgetter('lname', 'fname'))
pprint.pprint(rows_by_lname)
pprint.pprint(rows_by_fullname)
