from operator import attrgetter
from pprint import pprint


class User:
    def __init__(self, fn, ln):
        self.fn = fn
        self.ln = ln

    def __repr__(self):
        return f'User({self.fn} {self.ln})'


people = [User('sdfsdf', 'fsdfsd'), User('errtyt', 'cxvc'), User('fghhg', 'asdas')]

s_p = sorted(people, key=attrgetter('fn', 'ln'))
pprint(s_p)
