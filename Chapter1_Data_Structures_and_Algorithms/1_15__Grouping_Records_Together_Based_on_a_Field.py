from operator import itemgetter
from itertools import groupby
from pprint import pprint

rows = [
    {'address': '5412 N CLARK', 'date': '07/01/2012'},
    {'address': '5148 N CLARK', 'date': '07/04/2012'},
    {'address': '5800 E 58TH', 'date': '07/02/2012'},
    {'address': '2122 N CLARK', 'date': '07/03/2012'},
    {'address': '5645 N RAVENSWOOD', 'date': '07/02/2012'},
    {'address': '1060 W ADDISON', 'date': '07/02/2012'},
    {'address': '4801 N BROADWAY', 'date': '07/01/2012'},
    {'address': '1039 W GRANVILLE', 'date': '07/04/2012'},
]
# groupby() only examines consecutive items, failing to sort first won’t group the records as you want.
# Sort by the desired field first
rows.sort(key=itemgetter('date'))
gr = groupby(rows, key=itemgetter('date'))
for date, items in groupby(rows, key=itemgetter('date')):
    print(date)
    for i in items:
        print(' ', i)

# If your goal is to simply group the data together by dates into a large data structure that
# allows random access, you may have better luck using defaultdict()