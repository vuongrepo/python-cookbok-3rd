from collections import defaultdict

dict1 = defaultdict(list)
dict1['a'].append(1)
dict1['a'].append(21)
dict1['b'].append([1, 2])
print(dict1)

dict2 = {} #
dict2.setdefault('a', []).append(1)
dict2.setdefault('a', []).append(2)
print(dict2)