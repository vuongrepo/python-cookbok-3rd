import os

files = os.listdir('.')
if any(name.endswith('.py') for name in files):
    print('there is a python file')
else:
    print('no python files found')

num = ['XXVII', 0, 2]
strng = '='.join(str(i) for i in num)
print(strng)

portfolio = [
    {'name': 'GOOG', 'shares': 50},
    {'name': 'YHOO', 'shares': 75},
    {'name': 'AOL', 'shares': 20},
    {'name': 'SCOX', 'shares': 65}
]

minshares = min(s['shares'] for s in portfolio)
print(minshares)

min_item = min(portfolio, key=lambda x: x['shares'])
print(min_item)
