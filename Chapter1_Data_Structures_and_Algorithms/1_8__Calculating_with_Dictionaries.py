stocks = {
    'ACME': 45.23,
    'AAPL': 612.78,
    'IBM': 205.55,
    'HPQ': 37.20,
    'FB': 10.75
}

min_price = min(zip(stocks.values(), stocks.keys()))
sorted_prices = sorted(zip(stocks.values(), stocks.keys()), reverse=True)
print(min_price)
print(sorted_prices)