from collections import deque

q = deque(maxlen=3)
q.append(23)
q.append(24)
q.append(25)
q.append(26)
q.append(27)
print(q)

q.appendleft(109797)
print(q)

q.popleft()
print(q)
