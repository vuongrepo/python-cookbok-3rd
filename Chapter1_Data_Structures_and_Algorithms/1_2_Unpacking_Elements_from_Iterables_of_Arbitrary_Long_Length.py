tupl = ('Dave', 'dave@example.com', '773-555-1212', '847-555-1212')
name, mail, *phone = tupl
print(phone)

records = [
    ('foo', 1, 2),
    ('bar', 'hello'),
    ('foo', 3, 4),
]
for first, *rest in records:
    print(first, rest, sep='__')
