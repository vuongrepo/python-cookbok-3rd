import heapq


class PriorityQueue:
    def __init__(self):
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        return heapq.heappop(self._queue)[-1]


class Item:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f'Item({self.name})'


if __name__ == '__main__':
    pq = PriorityQueue()
    pq.push(Item('first'), 1)
    pq.push(Item('second'), 3)
    pq.push(Item('third'), 10)
    pq.push(Item('fourth'), 21)
    pq.push(Item('fifth'), 6)
    print(pq.pop())
